JavaScript
==========

About
-----

All js files that you will need are located in ``template/js`` folder

vendor.js
    contains vendor libraries (Bootstrap3, Jquery, Chosen, etc) already compiled into a single file, with
    versions that are verified to work with our theme. Normally, you should not edit that file.

app.js
    JavaScript library code that Realtyspace theme relies on, in order to function properly. Normally, you should not
    edit this file or add your own code there. This file exposes a very simple API that you can use in
    your own code.

demodata.js
    This file is used for demonstration purposes and contains example property items, that are mostly used to
    render markers on the map. You can safely delete this file, after you've adapted the demo.js code
    to use your own data.

demo.js
    the main file, that you should modify (or create another one). It contains lots of examples of
    plugin usage, with detailed comments about specific sections of the code. This is the file that runs
    most of the code defined in vendor.js and app.js, therefore you can easily make changes in this file,
    which will affect the way the theme works.


Here is the correct order to connect these JavaScript in your template:

.. code-block:: html

    <script type="text/javascript" src="https://maps.google.com/maps/api/js?libraries=places&amp;sensor=false"></script>
    <script type="text/javascript" src="js/vendor.js"></script>
    <script type="text/javascript" src="js/demodata.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
    <script type="text/javascript" src="js/demo.js"></script>


F.A.Q.
------

.. contents::
    :local:
    :depth: 1


.. _demojs:

...Can i use create my own file intead of demo.js?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Totally! We've created this file so you can look at examples of module and plugin initialization.
But if you know what you're doing,  you can remove the line

.. code-block:: html

    <script type="text/javascript" src="js/demo.js"></script>


from the template and connect instead your own file with the code that you need.

