Changelog
=========

1.2
---

- Visual-hierarchy improvememnt: Major changes in property block area in order to remove the crowded feeling, all the secondary information was removed or hidden in order to reveal on hover action
- Visual-hierarchy improvement: Removed secondary information in agent and news blocks in order to make them look more lightweight
- Visual-hierarchy improvement: Increased font-sizing and spacing between blocks and headers
- More improvements related to spacing, alignment and consistency
- Removed other unimportant information from theme and moved it to the dedicated item page.

1.1
---
- Added new "Welcome" block and frontpage
- Added new "Call to action" block on frontpage
- Redesigner main slide banner to a bigger and nicer version
- Major overhaul and redesign of UI blocks in order to follow along WebDesign Hierarchy principles
- Improved color contrast in all color themes
- Major improvements in domain of spacing and alignment issues between UI elements
- Performance improvements on smartphones
- Complete menu module rewrite, now it works even better on mobile devices

1.0
---
- First version